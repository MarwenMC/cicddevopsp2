import pytest
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.desired_capabilities import DesiredCapabilities
from selenium.webdriver import ChromeOptions

class Test_checkout():
  def setup_method(self, method):
    #configure chrome in headless to work in containers
    options = ChromeOptions()
    options.add_argument('--headless')
    options.add_argument('--disable-extensions')
    options.add_argument("--no-sandbox")
    self.driver = webdriver.Remote('http://localhost:4444/wd/hub', options.to_capabilities())

  
  def teardown_method(self, method):
    self.driver.quit()

  def test_test(self):
    self.driver.get("http://0.0.0.0:8000/")
    self.driver.set_window_size(853, 692)
    self.driver.find_element(By.CSS_SELECTOR, ".site-menu > li:nth-child(2) > a").click()
    self.driver.find_element(By.CSS_SELECTOR, ".block-4-image .img-fluid").click()
    self.driver.find_element(By.ID, "id_quantity").send_keys("1")
    self.driver.find_element(By.ID, "id_quantity").click()
    self.driver.find_element(By.ID, "id_colour").click()
    dropdown = self.driver.find_element(By.ID, "id_colour")
    dropdown.find_element(By.XPATH, "//option[. = 'green']").click()
    self.driver.find_element(By.CSS_SELECTOR, "#id_colour > option:nth-child(2)").click()
    self.driver.find_element(By.ID, "id_size").click()
    dropdown = self.driver.find_element(By.ID, "id_size")
    dropdown.find_element(By.XPATH, "//option[. = 's']").click()
    self.driver.find_element(By.CSS_SELECTOR, "#id_size > option:nth-child(2)").click()
    self.driver.find_element(By.CSS_SELECTOR, ".btn").click()
    self.driver.find_element(By.CSS_SELECTOR, ".form-control").click()
    element = self.driver.find_element(By.CSS_SELECTOR, ".form-control")
    assert element.is_enabled() is True
